## Descripción del sitio

Esta Web estática está ideada para realizar pequeños artículos y enlaces a temáticas diversas como tecnologías, sociedad o ciencias básicas. En realidad voy a escribir sobre lo que me interese en cada momento. Consulta la sección [About](page/about/) para saber algo más de mi.
